#! /usr/bin/python3
# coding: utf-8

######
##
## BabelBot, a cool cross-network translating IRC bot.
## Copyright (c) 2021 Sébastien “Elzen” Dufromentel
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##
######

import re
import cgi
import ssl
import json
import signal
import irc.bot
import threading
import collections
import urllib.request

chan, bots = "#play.it", {}
ips = collections.defaultdict(lambda:
    "2a00:5884:8300::01"+hex(len(ips))[2:].zfill(2))
servers = { "fr": "irc.oftc.net", "en": "irc.geeknode.org" }
ignore = set(irc.strings.IRCFoldedCase(n) for n in ("C", "ChanServ"))
trans = "https://libretranslate.de/translate"

def botnick(usernick, lang):
    if usernick in ignore:
        return None  # Nope.
    if usernick.endswith("]"):
        if usernick.endswith("fr]") or usernick.endswith("en]"):
            return None  # It was already a bot nick, not needed.
        return irc.strings.IRCFoldedCase(usernick[:-1]+"|"+lang+"]")
    return irc.strings.IRCFoldedCase(usernick+"["+lang+"]")

def usernick(botnick, lang):
    if not botnick.endswith(lang+"]"):
        return None  # Not a bot nick.
    nick = botnick[:-len(lang)-1]  # Cleaning…
    return irc.strings.IRCFoldedCase(nick[:-1]+"]"
        if nick.endswith("|") else nick[:-1])

def translate(message, target):
    if message == "\o/":
        return message
    num, words, repls = 1, [], {}
    for word in re.split("([a-zA-Z0-9_\[\]\-]+)", message):
        if irc.strings.IRCFoldedCase(word) in bots.keys():
            nick = usernick(word, target)
            if nick is None:  # Don't translate.
                words.append(word)
            else:  # Okay, let's tranlate it.
                repls["*"+str(num)] = nick
                words.append("*"+str(num))
                num += 1
        else:  # The opposite?
            nick = botnick(word, "fr" if target == "en" else "en")
            if nick in bots.keys():
                repls["*"+str(num)] = nick
                words.append("*"+str(num))
                num += 1
            else:  # Normal word.
                words.append(word)
    message = "".join(words)  # Should be good now.
    try:  # The server may have troubles to translate.
        with urllib.request.urlopen(trans, urllib.parse.urlencode({ "source":
                    "auto", "target": target, "q": message }).encode()) as f:
            message = cgi.html.unescape(json.loads(f.read())["translatedText"])
    except Exception:  # Probably some 500.
        pass  # Does it help to report it?
    for key, repl in repls.items():
        message = message.replace(key, repl)
    return message  # Should be translated?

class data(object):
    def __init__(self, **args):
        for name, arg in args.items():
            setattr(self, name, arg)

class Bot(irc.bot.SingleServerIRCBot):
    def __init__(self, nick, name):
        bots[nick], sockinfos = self, [ (servers[nick[-3:-1]], 6697) ]
        irc.bot.SingleServerIRCBot.__init__(self, sockinfos, nick, name, 5,
            connect_factory=irc.connection.Factory(bind_address=(ips[nick],
                6697), wrapper=ssl.SSLContext().wrap_socket, ipv6=True))
        threading.Thread(None, self.start, daemon=True).start()
    
    def on_welcome(self, conn, ev):
        self.connection.join(chan)
    
    def on_privmsg(self, conn, ev):
        pass #TODO answer that I'm just a bot.

class Babel(Bot):
    def on_endofnames(self, conn, ev):
        for nick in tuple(self.channels[chan].users()):
            self.on_join(conn, data(source=data(nick=nick)))
    
    def on_join(self, conn, ev):
        nick = botnick(ev.source.nick, self.botslang)
        if nick is not None:  # Let's work with it.
            if nick in bots.keys():  # Already here.
                return bots[nick].connection.join(chan)
            Bot(nick, ev.source.nick) #FIXME read name?
    
    def on_nick(self, conn, ev):
        self.on_quit(conn, ev)
        self.on_join(conn, data(source=data(nick=ev.target)))
    
    def on_quit(self, conn, ev):
        nick = botnick(ev.source.nick, self.botslang)
        if nick in bots.keys():
            bots[nick].connection.part((chan,))
    
    def on_part(self, conn, ev):
        self.on_quit(conn, ev)
    
    def on_kick(self, conn, ev):
        self.on_quit(conn, ev)
    
    def on_pubmsg(self, conn, ev):
       nick = botnick(ev.source.nick, self.botslang)
       if nick in bots.keys():  # Let's propagate the message…
           bots[nick].connection.privmsg(chan, translate(ev.arguments[0],
               "fr" if self.botslang == "en" else "en"))

Babel("BabelBot[fr]", "Cool translating bot").botslang = "en"
Babel("BabelBot[en]", "Chouette bot traducteur").botslang = "fr"

if __name__ == "__main__":
    signal.pause()
